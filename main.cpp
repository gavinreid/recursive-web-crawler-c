#include <iostream>
#include <map>
#include <regex>
#include <string>
#include <sstream>
#include <bits/stdc++.h>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>


int initalDepth = 0;
bool inTime = false;
int count = 2;
int mapSearch(std::string data, std::map< std::string, int> &mappy){return mappy.count(data);}


std::map< std::string, int> curl(std::string url, int maxDepth, std::map< std::string, int> &mappy, std::string domain){
    --maxDepth;
    if(maxDepth == -1){return mappy;}
    curlpp::Cleanup cleaner;
    curlpp::Easy request;
    curlpp::options::Url curlUrl(url);
    request.setOpt(curlUrl);
    curlpp::options::FollowLocation follow(true);
    request.setOpt(follow);
    std::ostringstream os;
    os << request << std::endl;
    std::string html(os.str());
    std::string retrieved;
    std::string re("<a[ \n\t]*[^(li)]href=\"(([^(mailto:)#](http)*[a-z]*[:]*[/]{0,2}[^(.exe)[][a-z.]*[^/])[^\"]*)\">");
    std::regex e(re);
    std::smatch m;
    std::string datax = domain;
    //URL Parser
    while (std::regex_search (html, m, e)) {
        std::string url = m[1];
        std::string domi = m[2];
        html = m.suffix().str(); // Careful this thing strips away the match
            // URL CLeanser
             if(url[0] == '/'){ url = domain + url; continue;}else{
                 if(domi[0] == '/') 
                    domi = domain;
             }
             
             if (mapSearch(url, mappy) == 0) {
                  if(inTime){
                   std::cout << "Depth: "<< (initalDepth - maxDepth);
                   
                  
                  for(int x = maxDepth; x <= initalDepth-1; x++){std::cout <<"    ";}
                     std::cout << url << '\n';}
                     mappy.insert(std::pair<std::string, int>(url, (initalDepth - maxDepth)));
                    
                     // EXECUTE RECURSIVE CALL
                     try { curl(url, maxDepth, mappy, domi);
                     } catch (const std::exception& e) {std::cout << e.what();}
             }
		}
		return mappy;
	}


int main(int argc, char** argv) {
    typedef std::map< std::string, int> urly;
    urly mappy;
    urly saved;
    char q, l;
    std::cout <<"Output results from hashmap after recursion press => 'h' | during recursion anything else: ";
    std:: cin >> l;
     if(l != 'h'){
        inTime = true;
        std::cout <<'\n' << "Fast output press f | normal live view anything else: ";
        std::cin >> q;
    if(q == 'f'){
      std::ios_base::sync_with_stdio(false);
      std::cin.tie(NULL);  
    }
    }
    
    std::cout << '\n';
   
    std::string there = "http://www.cs.usu.edu/";
    
    int maxDepth = 3;

    if (argc > 1)
        there = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);
        
    initalDepth = maxDepth;
    mappy.insert(std::pair<std::string, int>(there, 0));
    if(there.back() == '/'){there.pop_back();}
    
    std::string dom = there;
    std::cout << "Recursively crawling the web to a depth of " << maxDepth << " links beginning from " << there <<std::endl;
    std::cout << "Initalizing..."<< '\n';
 
    saved = curl(there, maxDepth, mappy, dom);
   
    std::cout <<'\n' << "Found: "<< saved.size() << " results" << '\n' << '\n' << '\n';
    
    if(!inTime){ for (const auto &p : saved) {std::cout <<"Depth: "<< p.second << "      "<<p.first << '\n';}}

    return 0;
}


 

